import Runner from '../runner.ts';

interface Position {
    x: number
    y: number
}

type Direction = 'U' | 'D' | 'R' | 'L'

class Bonus extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./09/input.txt')).split('\n')

        const visited = new Set<string>();

        const head: Position = { x: 0, y: 0 }
        const tail = new Array<Position>(9)
        for (let i = 0; i < tail.length; i++) {
            tail[i] = { x: 0, y: 0 }
        }
        visited.add(JSON.stringify(tail[tail.length - 1]))
        for (const item of input) {
            const commands = item.split(' ')
            const direction: Direction = commands[0] as Direction
            const steps = parseInt(commands[1])

            for (let i = 0; i < steps; i++) {
                this.Move(head, direction)

                this.Follow(tail[0], head)
                for (let j = 1; j < tail.length; j++) {
                    this.Follow(tail[j], tail[j - 1])
                }

                visited.add(JSON.stringify(tail[tail.length - 1]))
            }
        }

        return visited.size.toString()
    }

    private Move(position: Position, direction: Direction) {
        switch (direction) {
            case 'U':
                position.y--
                break
            case 'D':
                position.y++
                break
            case 'L':
                position.x--
                break
            case 'R':
                position.x++
                break
        }
    }

    private Follow(from: Position, to: Position) {
        if (Math.abs(from.x - to.x) > 1 || Math.abs(from.y - to.y) > 1) {
            if (from.x != to.x) {
                from.x += from.x > to.x ? -1 : 1;
            }
            if (from.y != to.y) {
                from.y += from.y > to.y ? -1 : 1;
            }
        }
    }
}

export default Bonus
