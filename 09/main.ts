import Runner from '../runner.ts';

interface Position {
    x: number
    y: number
}

type Direction = 'U' | 'D' | 'R' | 'L'

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./09/input.txt')).split('\n')

        const visited = new Set<string>();

        const tail: Position = { x: 0, y: 0 }
        const head: Position = { x: 0, y: 0 }
        visited.add(JSON.stringify(tail))
        for (const item of input) {
            const commands = item.split(' ')
            const direction: Direction = commands[0] as Direction
            const steps = parseInt(commands[1])

            for (let i = 0; i < steps; i++) {
                this.Move(head, direction)
                this.Follow(tail, head)

                visited.add(JSON.stringify(tail))
            }
        }

        return visited.size.toString()
    }

    private Move(position: Position, direction: Direction) {
        switch (direction) {
            case 'U':
                position.y--
                break
            case 'D':
                position.y++
                break
            case 'L':
                position.x--
                break
            case 'R':
                position.x++
                break
        }
    }

    private Follow(from: Position, to: Position) {
        if (Math.abs(from.x - to.x) > 1 || Math.abs(from.y - to.y) > 1) {
            if (from.x != to.x) {
                from.x += from.x > to.x ? -1 : 1
            }
            if (from.y != to.y) {
                from.y += from.y > to.y ? -1 : 1
            }
        }
    }
}

export default Main
