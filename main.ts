import { parse } from "https://deno.land/std@0.167.0/flags/mod.ts"
import Runner from './runner.ts'

/**
 * Runs all the typescript files of the day folder
 * @param day Day to run
 */
async function RunDay(day: number) {
    const path = `./${day.toString().padStart(2, '0')}`

    try {
        // Check if folder exists
        Deno.readDirSync(path)
    } catch (error) {
        if (error instanceof Deno.errors.NotFound) {
            console.log('Day not found');
        }
        else {
            console.error(error);
        }
        return
    }

    console.log(`Day ${day}`);
    const files = Array.from(Deno.readDirSync(path))
        .filter(x => x.isFile && x.name.slice((x.name.lastIndexOf(".") - 1 >>> 0) + 2) === 'ts')
        .map(x => x.name)
        .sort((a, b) => b.localeCompare(a))
    for (const item of files) {
        const name = item[0].toUpperCase() + item.substring(1, item.length - 3)
        try {
            const Module = new ((await import(`${path}/${item}`)).default)() as Runner
            console.log(`${name}: ${await Module.Run()}`)

        } catch (error) {
            if (error instanceof Deno.errors.NotFound) {
                console.log(`${name} not found`);
            }
            else {
                console.error(error);
            }
        }
    }
}

let { day } = parse(
    Deno.args,
    {
        default: { day: -1 },
        alias: { d: 'day' }
    }
)

if (day === -1) {
    let exit = false;
    const folders = Array.from(Deno.readDirSync('.'))
        .filter(x => x.isDirectory && !isNaN(parseInt(x.name)))
        .map(x => parseInt(x.name))
        .sort((a, b) => a - b)
    while (!exit) {
        for (const item of folders) {
            console.log(`Day ${item}`);
        }
        const input = prompt('Please enter a day')

        if (input) {
            try {
                const value = parseInt(input)
                if (value === -1) {
                    Deno.exit(0)
                }

                if (folders.find(x => x === value)) {
                    day = value
                    exit = true
                }
                else {
                    console.error('Day not found');
                }
            } catch (_error) {
                console.error('Expecting a number');
            }
        }
        else {
            console.error('Expecting a input');
        }

        console.log();
    }
}

await RunDay(day)
