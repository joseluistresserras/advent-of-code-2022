import Runner from '../runner.ts';

interface Node {
    position: number
    distance: number
}

class Bonus extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./12/input.txt')).replace('S', 'a')
        const start = new Array<number>()
        const end = input.replaceAll('\n', '').indexOf('E')

        const map = input.replace('E', 'z').split('\n')

        const graph = new Array<Array<number>>()
        for (let i = 0; i < map.length; i++) {
            for (let j = 0; j < map[i].length; j++) {
                const item = map[i].charCodeAt(j);

                if (item === 97) {
                    start.push(i * map[i].length + j)
                }

                const edges = new Array<number>()
                if (i - 1 >= 0 && map[i - 1].charCodeAt(j) - item <= 1) {
                    edges.push(((i - 1) * map[i].length) + j);
                }
                if (i + 1 < map.length && map[i + 1].charCodeAt(j) - item <= 1) {
                    edges.push(((i + 1) * map[i].length) + j);
                }

                if (j - 1 >= 0 && map[i].charCodeAt(j - 1) - item <= 1) {
                    edges.push((i * map[i].length) + (j - 1));
                }
                if (j + 1 < map[i].length && map[i].charCodeAt(j + 1) - item <= 1) {
                    edges.push((i * map[i].length) + (j + 1));
                }

                graph.push(edges)
            }
        }

        let min_distance = Infinity
        for (const element of start) {
            const visited = new Array<boolean>(graph.length).fill(false)
            const queue = new Array<Node>()

            visited[element] = true
            queue.push({ position: element, distance: 0 })

            let result = Infinity
            let exit = false
            while (!exit && queue.length > 0) {
                const node = queue.shift()!

                if (node.position === end) {

                    result = node.distance
                    exit = true
                }
                else {
                    const adjacents = graph[node.position]
                    for (const item of adjacents) {
                        if (!visited[item]) {
                            queue.push({ position: item, distance: node.distance + 1 })
                            visited[item] = true
                        }
                    }
                }
            }

            if (exit && result < min_distance) {
                min_distance = result
            }
        }

        if (min_distance != Infinity) {
            return min_distance.toString()
        }

        return 'No path found'
    }
}

export default Bonus
