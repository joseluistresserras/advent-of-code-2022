import Runner from '../runner.ts';

interface Node {
    position: number
    distance: number
}

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./12/input.txt'))
        const start = input.replaceAll('\n', '').indexOf('S')
        const end = input.replaceAll('\n', '').indexOf('E')

        const map = input.replace('S', 'a').replace('E', 'z').split('\n')

        const graph = new Array<Array<number>>()
        for (let i = 0; i < map.length; i++) {
            for (let j = 0; j < map[i].length; j++) {
                const item = map[i].charCodeAt(j);

                const edges = new Array<number>()
                if (i - 1 >= 0 && map[i - 1].charCodeAt(j) - item <= 1) {
                    edges.push(((i - 1) * map[i].length) + j);
                }
                if (i + 1 < map.length && map[i + 1].charCodeAt(j) - item <= 1) {
                    edges.push(((i + 1) * map[i].length) + j);
                }

                if (j - 1 >= 0 && map[i].charCodeAt(j - 1) - item <= 1) {
                    edges.push((i * map[i].length) + (j - 1));
                }
                if (j + 1 < map[i].length && map[i].charCodeAt(j + 1) - item <= 1) {
                    edges.push((i * map[i].length) + (j + 1));
                }

                graph.push(edges)
            }
        }

        const visited = new Array<boolean>(graph.length).fill(false)
        const queue = new Array<Node>()

        visited[start] = true
        queue.push({ position: start, distance: 0 })

        let result = Infinity
        let exit = false
        while (!exit && queue.length > 0) {
            const node = queue.shift()!

            if (node.position === end) {

                result = node.distance
                exit = true
            }
            else {
                const adjacents = graph[node.position]
                for (const item of adjacents) {
                    if (!visited[item]) {
                        queue.push({ position: item, distance: node.distance + 1 })
                        visited[item] = true
                    }
                }
            }
        }

        if (result != Infinity) {
            return result.toString()
        }

        return 'No path found'
    }
}

export default Main
