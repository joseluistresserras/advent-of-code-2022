# Advent of Code 2022 by Jose Luis Tresserras Merino

You can run the code with Deno.
```ts
deno run --allow-read ./main.ts
deno run --allow-read ./main.ts -d 1
deno run --allow-read ./main.ts --day 1
```
You can choose an specific day to run!

## https://adventofcode.com/2022/
