import Runner from "../runner.ts";

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile("./03/input.txt")).split('\n')

        let priority = 0
        for (const item of input) {
            const rack_length = item.length / 2
            let exit = false
            let i = 0
            while (!exit && i < rack_length) {
                let j = rack_length;
                while (!exit && j < item.length) {
                    if (item[i] === item[j]) {
                        priority += item.charCodeAt(i) + (item[i].toLowerCase() == item[i] ? -97 + 1 : -65 + 27)
                        exit = true
                    }

                    j++
                }

                i++
            }
        }

        return priority.toString()
    };
}

export default Main
