import Runner from "../runner.ts";

class Bonus extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile("./03/input.txt")).split('\n')

        let priority = 0
        for (let i = 0; i < input.length; i += 3) {
            const item = input[i]

            let exit = false
            let j = 0
            const rucksack1 = input[i + 1]
            const rucksack2 = input[i + 2]
            while (!exit && j < item.length) {
                let k = 0
                while (!exit && k < rucksack1.length) {
                    if (item[j] === rucksack1[k]) {
                        let m = 0
                        while (!exit && m < rucksack2.length) {
                            if (item[j] === rucksack2[m]) {
                                priority += item.charCodeAt(j) + (item[j].toLowerCase() == item[j] ? -97 + 1 : -65 + 27)
                                exit = true
                            }

                            m++
                        }
                    }

                    k++
                }

                j++
            }
        }

        return priority.toString()
    };
}

export default Bonus
