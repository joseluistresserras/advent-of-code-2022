import Runner from '../runner.ts';

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./13/input.txt')).split('\n')
        let sum = 0

        for (let i = 0; i < input.length; i += 3) {
            const left = input[i]
            const right = input[i + 1]

            const left_ip = this.ParseInput(left)
            const right_ip = this.ParseInput(right)

            if (this.Compare(left_ip, right_ip) === 1) {
                sum += (i / 3) + 1
            }
        }

        return sum.toString()
    }

    private ParseInput(input: string) {
        const ip = new ItemParsed()

        input = input.substring(1, input.length - 1)


        let exit = input.length <= 0
        while (!exit) {
            let index
            if (input[0] === '[') {
                index = this.IndexClosingBracket(input) + 1
                ip.Values.push(this.ParseInput(input.substring(0, index)))
            }
            else {
                index = input.indexOf(',')
                if (index === -1) {
                    index = input.length
                }

                ip.Values.push(parseInt(input.substring(0, index)))
            }
            input = input.substring(index)

            if (input.length > 0) {
                input = input.substring(1)
            }
            else {
                exit = true
            }
        }

        return ip
    }

    private Compare(left: ItemParsed, right: ItemParsed) {
        let retVal = -1
        let count = 0
        while (retVal < 0 && count < left.Values.length && count < right.Values.length) {
            if (left.Values[count] instanceof ItemParsed || right.Values[count] instanceof ItemParsed) {
                let new_left = left.Values[count]
                let new_right = right.Values[count]

                if (!(new_left instanceof ItemParsed)) {
                    const ip = new ItemParsed()
                    ip.Values.push(new_left)
                    new_left = ip
                }
                else if (!(new_right instanceof ItemParsed)) {
                    const ip = new ItemParsed()
                    ip.Values.push(new_right)
                    new_right = ip
                }

                retVal = this.Compare(new_left as ItemParsed, new_right as ItemParsed)
            }
            else {
                if (left.Values[count] < right.Values[count])
                {
                    retVal = 1;
                }
                else if (left.Values[count] > right.Values[count])
                {
                    retVal = 0;
                }
            }

            count++
        }

        if (retVal < 0) {
            if (left.Values.length < right.Values.length)
            {
                retVal = 1;
            }
            else if (left.Values.length > right.Values.length)
            {
                retVal = 0;
            }
        }

        return retVal
    }

    private IndexClosingBracket(input: string) {
        let exit = false
        let count = 1
        let level = 1
        let index = -1

        while (!exit && count < input.length) {
            if (input[count] === '[') {
                level++
            }
            else if (input[count] === ']') {
                level--
            }

            if (level <= 0) {
                index = count
                exit = true
            }

            count++
        }

        return index
    }
}

class ItemParsed {
    public readonly Values: Array<ItemParsed | number>

    constructor() {
        this.Values = new Array<ItemParsed | number>()
    }
}

export default Main
