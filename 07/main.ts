import Runner from '../runner.ts';

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./07/input.txt')).split('\n')

        let structure = new Resource('/');
        for (const item of input) {
            const line = item.split(' ');
            if (item[0] === '$') {
                if (line[1] === 'cd') {
                    const dir = line[2];
                    switch (dir) {
                        case '/':
                            while (structure.Previous != null) {
                                structure = structure.Previous;
                            }
                            break;
                        case '..':
                            if (structure.Previous) {
                                structure = structure.Previous;
                            }
                            break;
                        default:
                            structure = structure.Resources.find(x => x.Name == dir)!;
                            break;
                    }
                }
            }
            else {
                if (line[0] === 'dir') {
                    structure.Resources.push(new Resource(line[1], 0, structure));
                }
                else {
                    structure.Resources.push(new Resource(line[1], parseInt(line[0]), structure));
                }
            }
        }

        while (structure.Previous != null) {
            structure = structure.Previous;
        }

        return structure.GetAtMost(100000).toString()
    };
}

class Resource {
    public readonly Name: string
    public readonly Size: number
    public readonly Previous?: Resource
    public readonly Resources: Array<Resource>

    constructor(name: string, size: number = 0, previous?: Resource) {
        this.Name = name
        this.Size = size
        this.Previous = previous
        this.Resources = new Array<Resource>()
    }

    GetSize(): number {
        return this.Size + this.Resources.map(x => x.GetSize()).reduce((a, b) => a + b, 0);
    }

    Show(count = 0) {
        console.log(`${' '.repeat(count)}- ${this.Name}: ${this.GetSize()}`);

        for (const item of this.Resources) {
            item.Show(count + 1);
        }
    }

    GetAtMost(at_most: number) {
        if (this.Size == 0) {
            const size = this.GetSize();
            let retVal = 0;
            for (const item of this.Resources) {
                retVal += item.GetAtMost(at_most);
            }
            if (size > at_most) {
                return retVal;
            }
            else {
                return size + retVal;
            }
        }

        return 0;
    }
}

export default Main
