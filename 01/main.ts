import Runner from "../runner.ts";

class Main extends Runner {
    public readonly Run = async () => {
        const text = (await Deno.readTextFile("./01/input.txt")).split('\n')
        text.push('')

        let calories = 0
        let max_calories = 0
        for (const item of text) {
            if (item == '') {
                if (max_calories < calories) {
                    max_calories = calories
                }

                calories = 0
            }
            else {
                calories += parseInt(item)
            }
        }

        return max_calories.toString()
    };
}

export default Main
