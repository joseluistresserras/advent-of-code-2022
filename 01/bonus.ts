import Runner from "../runner.ts"

class Bonus extends Runner {
    public readonly Run = async () => {
        const text = (await Deno.readTextFile("./01/input.txt")).split('\n')
        text.push('')

        const top_calories = [0, 0, 0]

        let calories = 0
        for (const item of text) {
            if (item == '') {
                // Get minimum value and position
                let min_pos = 0
                let min_value = top_calories[min_pos]
                for (let i = 0; i < top_calories.length; i++) {
                    const element = top_calories[i];
                    if (element < min_value) {
                        min_pos = i
                        min_value = element
                    }
                }

                // If calories of elf are bigger of the minimum value, override position
                if (min_value < calories) {
                    top_calories[min_pos] = calories
                }

                calories = 0
            }
            else {
                calories += parseInt(item)
            }
        }

        return top_calories.reduce((prev, curr) => prev + curr, 0).toString()
    };
}

export default Bonus
