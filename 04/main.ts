import Runner from "../runner.ts";

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile("./04/input.txt")).split('\n')

        let duplicated = 0
        for (const item of input) {
            const pair = item.split(',')

            const first = pair[0].split('-')
            const second = pair[1].split('-')
            if (
                parseInt(first[0]) <= parseInt(second[0]) && parseInt(first[1]) >= parseInt(second[1]) ||
                parseInt(second[0]) <= parseInt(first[0]) && parseInt(second[1]) >= parseInt(first[1])
            ) {
                duplicated++;
            }
        }

        return duplicated.toString()
    };
}

export default Main
