import Runner from "../runner.ts";

class Bonus extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile("./06/input.txt"))

        let count = 0
        const max = 14
        let exit = false
        while (!exit && count < input.length) {
            let i = count
            while (!exit && i < count + max - 1) {
                let j = i + 1
                while (!exit && j < count + max) {
                    if (input[i] == input[j]) {
                        exit = true
                    }

                    j++
                }

                i++
            }

            exit = !exit
            count++
        }

        count += max - 1

        return count.toString()
    };
}

export default Bonus
