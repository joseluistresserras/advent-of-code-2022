import Runner from "../runner.ts";

class Bonus extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile("./05/input.txt")).split('\n')

        let count = 0
        while (input[count].includes('[') && count < input.length) {
            count++
        }

        const stacks = Array<Array<string>>()
        const line = input[count]
        for (let i = 1; i < line.length; i += 4) {
            const stack = new Array<string>();

            let j = count - 1
            while (j >= 0 && input[j][i] != ' ') {
                stack.push(input[j][i])

                j--
            }

            stacks.push(stack)
        }

        count += 2
        for (let i = count; i < input.length; i++) {
            const item = input[i].split(' ')
            const move = parseInt(item[1])
            const orig = parseInt(item[3]) - 1
            const dest = parseInt(item[5]) - 1

            const stack: string[] = []
            for (let j = 0; j < move; j++) {
                stack.push(stacks[orig].pop()!)
            }

            while (stack.length > 0) {
                stacks[dest].push(stack.pop()!)
            }
        }

        return stacks.map(x => x[x.length - 1]).join('')
    };
}

export default Bonus
