import Runner from '../runner.ts';

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./11/input.txt')).split('\n')
        input.push('')

        const monkeys = new Array<Monkey>()
        for (let i = 0; i < input.length; i += 7) {
            const id = parseInt(input[i].split(' ')[1].replace(':', ''))
            const starting_items = input[i + 1].split(': ')[1].split(', ').map(x => parseInt(x))
            const operation = input[i + 2].split(': new = ')[1]
            const condition = parseInt(input[i + 3].split('divisible by ')[1])
            const test: MonkeyTest = {
                divisor: condition,
                cases: [
                    parseInt(input[i + 4].split('throw to monkey ')[1]),
                    parseInt(input[i + 5].split('throw to monkey ')[1])
                ]
            }

            monkeys.push(
                new Monkey(
                    id,
                    starting_items,
                    operation,
                    test
                )
            )
        }

        const rounds = 20
        for (let i = 1; i <= rounds; i++) {
            for (const item of monkeys) {
                while (item.StartingItems.length > 0) {
                    const old = item.StartingItems.shift()!
                    item.Inspected++

                    const worry = eval(item.Operation.replace('old', old.toString()))
                    const borred = Math.floor(worry / 3)
                    const id = item.Test.cases[borred % item.Test.divisor === 0 ? 0 : 1]

                    monkeys[id].StartingItems.push(borred)
                }
            }
        }

        return monkeys
            .sort((a, b) => b.Inspected - a.Inspected)
            .map(x => x.Inspected)
            .slice(0, 2)
            .reduce((a, b) => a * b)
            .toString()
    }
}

interface MonkeyTest {
    divisor: number
    cases: [number, number]
}

class Monkey {
    public readonly Id: number
    public readonly StartingItems: Array<number>
    public readonly Operation: string
    public readonly Test: MonkeyTest
    public Inspected: number

    constructor(id: number, starting_items: Array<number>, operation: string, test: MonkeyTest) {
        this.Id = id
        this.StartingItems = starting_items
        this.Operation = operation
        this.Test = test
        this.Inspected = 0
    }
}

export default Main
