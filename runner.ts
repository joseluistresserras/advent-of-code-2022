/**
 * Default class to abstact Run method
 */
export default abstract class Runner {
    public readonly abstract Run: () => Promise<string>
}
