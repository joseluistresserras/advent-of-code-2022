import Runner from '../runner.ts';

class Bonus extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./08/input.txt')).split('\n')

        let max = 0
        for (let i = 1; i < input.length - 1; i++) {
            for (let j = 1; j < input[i].length - 1; j++) {
                const element = parseInt(input[i][j])

                const column = input.map(value => value[j]).join('')
                const blocked = [
                    this.GetBlock(this.Reverse(column.slice(0, i)), element),
                    this.GetBlock(this.Reverse(input[i].slice(0, j)), element),
                    this.GetBlock(column.slice(i + 1), element),
                    this.GetBlock(input[i].slice(j + 1), element),
                ]

                const count = blocked.reduce((value, current) => value * current)
                if (max < count) {
                    max = count
                }
            }
        }

        return max.toString()
    }

    private GetBlock(input: string, element: number): number {
        let visible = true
        let count = 0
        let value = 0
        while (visible && count < input.length) {
            value = parseInt(input[count])
            if (element <= value) {
                visible = false
            }
            count++
        }
        return count
    }

    private Reverse(input: string): string {
        return input.split('').reverse().join('')
    }
}

export default Bonus
