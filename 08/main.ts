import Runner from '../runner.ts';

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./08/input.txt')).split('\n')

        let visible = input[0].length * 2 + ((input.length - 2) * 2)
        for (let i = 1; i < input.length - 1; i++) {
            for (let j = 1; j < input[i].length - 1; j++) {
                const element = parseInt(input[i][j])

                const column = input.map(value => value[j]).join('')
                if (
                    this.IsVisible(column.slice(0, i), element) ||
                    this.IsVisible(column.slice(i + 1), element) ||
                    this.IsVisible(input[i].slice(0, j), element) ||
                    this.IsVisible(input[i].slice(j + 1), element)
                ) {
                    visible++
                }
            }
        }

        return visible.toString()
    }

    private IsVisible(input: string, element: number): boolean {
        let visible = true
        let count = 0
        let value = 0
        while (visible && count < input.length) {
            value = parseInt(input[count])
            if (element <= value) {
                visible = false
            }
            count++
        }
        return value < element
    }
}

export default Main
