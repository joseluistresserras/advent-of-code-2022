import Runner from "../runner.ts";

class Bonus extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile("./02/input.txt")).split('\n')
        let score = 0
        // ASCII Table
        // A => 65
        // X => 88
        for (const item of input) {
            const opponent = item.charCodeAt(0) - 65

            switch (item[2]) {
                case 'Y':
                    // Draw
                    score += 3 + opponent
                    break;
                case 'Z':
                    // Win
                    score += 6
                    if (opponent < 2) {
                        score += opponent + 1
                    }
                    break;
                default:
                    score += opponent == 0 ? 2 : opponent - 1
                    break;
            }

            score++
        }

        return score.toString()
    };
}

export default Bonus
