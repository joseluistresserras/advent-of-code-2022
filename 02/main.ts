import Runner from "../runner.ts";

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile("./02/input.txt")).split('\n')
        let score = 0
        // ASCII Table
        // A => 65
        // X => 88
        const diff = 23 // => 88 - 65 => 'X' - 'A';
        for (const item of input)
        {
            const opponent = item.charCodeAt(0) + diff
            const selected = item.charCodeAt(2)
            score += selected - 88 + 1
        
            if (opponent == selected)
            {
                // Draw
                score += 3
            }
            else if (opponent + 1 == selected || opponent - 2 == selected)
            {
                // Win
                score += 6
            }
            // else loose
        }
        
        return score.toString()
    };
}

export default Main
