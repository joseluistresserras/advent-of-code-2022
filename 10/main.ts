import Runner from '../runner.ts';

class Main extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./10/input.txt')).split('\n')

        let cycle = 1
        let register = 1
        let strength = 0
        let read = 20
        let reset = true
        for (let i = 0; i < input.length; i++) {
            const commands = input[i].split(' ')

            if (cycle === read) {
                strength += read * register

                read += 40
            }

            cycle++
            if (commands[0] == 'addx') {
                if (reset) {
                    i--
                }
                else {
                    register += parseInt(commands[1])
                }
                reset = !reset
            }
        }

        return strength.toString()
    }
}

export default Main
