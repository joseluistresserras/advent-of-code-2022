import Runner from '../runner.ts';

class Bonus extends Runner {
    public readonly Run = async () => {
        const input = (await Deno.readTextFile('./10/input.txt')).split('\n')

        let cycle = 1
        let register = 1
        let read = 41
        let reset = true
        const result = new Array<string>()
        let line = ''
        for (let i = 0; i < input.length; i++) {
            const commands = input[i].split(' ')

            if (cycle === read) {
                result.push(line)
                line = ''
                read += 40
            }

            line += Math.abs(register - line.length) > 1 ? ' ' : '#'

            cycle++
            if (commands[0] == 'addx') {
                if (reset) {
                    i--
                }
                else {
                    register += parseInt(commands[1])
                }
                reset = !reset
            }
        }
        result.push(line)

        return '\n' + result.join('\n')
    }
}

export default Bonus
